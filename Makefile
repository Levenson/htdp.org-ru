#

PROJECT = htdp

SCRIBBLE_OUTPUT = obj/
SCRIBBLE_FLAGS =  ++main-xref-in
GITLAB_PUBLIC = public

default:
	# mkdir -p $(SCRIBBLE_OUTPUT)
	# scribble $(SCRIBBLE_FLAGS) --prefix scribble-prefix.tex --style scribble-style.tex --xelatex --dest $(SCRIBBLE_OUTPUT) $(PROJECT).scrbl
	scribble $(SCRIBBLE_FLAGS) --redirect "https://docs.racket-lang.org/local-redirect/index.html" --htmls --dest $(GITLAB_PUBLIC) $(PROJECT).scrbl

display: default
	# (${PDFVIEWER} obj/$(PROJECT).pdf &)
	(${BROWSER} $(GITLAB_PUBLIC)/$(PROJECT)/index.html &)

### Standard PDF Viewers
# Defines a set of standard PDF viewer tools to use when displaying the result
# with the display target. Currently chosen are defaults which should work on
# most linux systems with GNOME installed and on all OSX systems.

UNAME := $(shell uname)
BROWSER = xdg-open

ifeq ($(UNAME), Linux)
PDFVIEWER = evince
endif

ifeq ($(UNAME), Darwin)
PDFVIEWER = open
endif


clean::
	rm -rf $(SCRIBBLE_OUTPUT)
