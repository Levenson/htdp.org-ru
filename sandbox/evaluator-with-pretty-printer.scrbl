#lang scribble/manual

@(require racket/pretty racket/math
          racket/sandbox
          scribble/example
          (for-label racket))

This is a test @racket[(list #true)]

Text

@(define my-evaluator
   (parameterize ([sandbox-output 'string]
                  [pretty-print-show-inexactness #t]
                  [print-boolean-long-form #t]
                  [sandbox-error-output 'string])
     (make-evaluator 'racket/base)))

@examples[#:eval my-evaluator
          (+ 2 4)
          #true
          #false
          #i-1.0
          e
          (cos e)]

@(racketblock
  (define (loop x)
    (loop (not x)))

  #true

  #false)
