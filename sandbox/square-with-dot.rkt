#lang racket

(require 2htdp/image)

(define (square-with-dot x y)
  (place-image (circle 3 "solid" "red")
               x y
               (empty-scene 50 50)))

(define (square-with-question-mark)
  (place-image (circle 3 "solid" "red")
               50 50
               (empty-scene 50 50)))
