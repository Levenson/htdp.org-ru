#lang scribble/manual

@(require scribble/example
          2htdp/image
          racket/sandbox)

@(define htdp-sandbox
   (parameterize ([sandbox-output 'string])
     (make-base-eval)))

@examples[#:hidden #:eval htdp-sandbox
          (require 2htdp/image)
          (define my-rocket
            (bitmap/file "img/rocket.png"))
          ]

@(define (my-rocket)
   (bitmap/file "img/rocket.png"))

@examples[#:label #f #:lang htdp-sandbox #:escape
          (eval:alts (place-image #,my-rocket 50 23 (empty-scene 100 60))
                     (place-image my-rocket 50 23 (empty-scene 100 60)))]

@examples[#:label #f #:lang htdp-sandbox #:escape
          (place-image #,my-rocket 50 23 (empty-scene 100 60))]
    
