#lang scribble/manual

@(require "htdp.rkt")

@title[#:date "January 2018"]{Как Писать Программы}

@margin-note{Пожалуйста сообщайте об ошибках по адресу @tt|{authors @ htdp.org}|.}

@margin-note{Оригинал книги доступен по @hyperlink["https://www.htdp.org"]{этому} адресу}

@(linebreak)
@(linebreak)

Matthias Felleisen, Robert Bruce Findler, Matthew Flatt, Shriram Krishnamurthi

@(linebreak)
@(linebreak)

@include-section["preface.scrbl"]
@include-section["prologue.scrbl"]

@include-section["chapter-1.scrbl"]
@include-section["intermezzo-1.scrbl"]

@; @include-section["chapter-2.scrbl"]
@; @include-section["intermezzo-2.scrbl"]

@; @include-section["chapter-3.scrbl"]
@; @include-section["intermezzo-3.scrbl"]

@; @include-section["chapter-4.scrbl"]
@; @include-section["intermezzo-4.scrbl"]

@; @include-section["chapter-5.scrbl"]
@; @include-section["intermezzo-5.scrbl"]

@; @include-section["chapter-6.scrbl"]

@; @include-section["epilog.scrbl"]


1 Август 2014 @hyperlink["https://mitpress.mit.edu"]{MIT Press} Этот
материал защищен авторским правом и представляется в рамках лицензии
Creative Commons
@hyperlink["https://creativecommons.org/licenses/by-nc-nd/2.0/legalcode"]{CC
BY-NC-ND}
[@hyperlink["https://creativecommons.org/licenses/by-nc-nd/2.0/"]{трактовка}].
